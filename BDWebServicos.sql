CREATE DATABASE [BDWebServicos]
GO
USE [BDWebServicos]
GO
CREATE TABLE [dbo].[tb_agenda_servico](
	[codigo] [int] IDENTITY(1,1) NOT NULL,
	[anuncio] [varchar](50) NULL,
	[codigoUsuario] [int] NULL,
	[preco] [decimal](18, 0) NULL,
	[data] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
CREATE TABLE [dbo].[tb_feedback](
	[codigo] [int] IDENTITY(1,1) NOT NULL,
	[comentario] [varchar](500) NULL,
	[usuario] [varchar](100) NULL,
	[data] [datetime] NULL DEFAULT (getdate()),
PRIMARY KEY CLUSTERED 
(
	[codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
CREATE TABLE [dbo].[tb_servico](
	[codigo] [int] IDENTITY(1,1) NOT NULL,
	[tipoServico] [int] NULL,
	[anuncio] [varchar](50) NULL,
	[codigoUsuario] [int] NULL,
	[preco] [decimal](18, 0) NULL,
	[descricao] [varchar](500) NULL,
	[dataCriacao] [datetime] NULL DEFAULT (getdate()),
	[dataAlteracao] [datetime] NULL DEFAULT (getdate()),
PRIMARY KEY CLUSTERED 
(
	[codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
CREATE TABLE [dbo].[tb_tipo_servico](
	[codigo] [int] IDENTITY(1,1) NOT NULL,
	[nomeServico] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

INSERT INTO tb_tipo_servico 
VALUES ('Limpeza'), ('Elétrico'), ('Pintura'), ('Marcenaria'), ('Obras'), ('Reparos'), ('Transporte'), ('Outros')
GO
CREATE TABLE [dbo].[tb_usuario](
	[codigo] [int] IDENTITY(1,1) NOT NULL,
	[login] [varchar](50) NOT NULL,
	[senha] [varchar](50) NOT NULL,
	[nome] [varchar](100) NULL,
	[email] [varchar](100) NOT NULL,
	[sexo] [char](1) NULL,
	[telefone] [int] NULL,
	[cpf] [varchar](50) NULL,
	[endereco] [varchar](100) NULL,
	[dataCadastro] [datetime] NULL DEFAULT (getdate()),
	[dataAlteracao] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

--BUSCA

CREATE PROCEDURE [dbo].[spBuscaServico](
@codigo INT
)AS
BEGIN
    SELECT codigo, tipoServico, anuncio, codigoUsuario, preco, descricao, dataCriacao, dataAlteracao 
	FROM tb_servico 
	WHERE codigo = @codigo
END



GO
 
--BUSCA
CREATE PROCEDURE [dbo].[spBuscaUsuario](
    @codigo INT
)AS
BEGIN
    SELECT codigo, login, senha, nome, email, sexo, telefone, cpf, endereco, dataCadastro, dataAlteracao
    FROM tb_usuario WHERE codigo = @codigo 
END

GO

--DELETE
CREATE PROCEDURE [dbo].[spDeleteFeedback](
    @codigo INT
)AS
BEGIN
    DELETE FROM tb_feedback WHERE codigo = @codigo     
END

GO

--DELETE
CREATE PROCEDURE [dbo].[spDeleteServico](
    @codigo INT
)AS
BEGIN
    DELETE FROM tb_servico WHERE codigo = @codigo     
END

GO

--DELETE
CREATE PROCEDURE [dbo].[spDeleteUsuario](
    @codigo INT
)AS
BEGIN
    DELETE FROM tb_usuario WHERE codigo = @codigo     
END

GO

--======================FEEDBACK

--INSERT
CREATE PROCEDURE [dbo].[spInsertFeedback](
    @comentario VARCHAR(500),
	@usuario VARCHAR(100)
)AS
BEGIN
    INSERT INTO tb_feedback(comentario, usuario, data) 
    OUTPUT inserted.codigo
	VALUES (@comentario, @usuario, GETDATE())
END

GO

--======================SERVIÇO

--INSERT
CREATE PROCEDURE [dbo].[spInsertServico](
    @tipoServico INT,
    @anuncio VARCHAR(50),
    @codigoUsuario INT,
    @preco DECIMAL(18,0),
    @descricao VARCHAR(500)
)AS
BEGIN
    INSERT INTO tb_servico(tipoServico, anuncio, codigoUsuario, preco, descricao, dataCriacao)
   
    VALUES (@tipoServico, @anuncio, @codigoUsuario, @preco, @descricao, GETDATE())
END
GO


GO

--======================USUARIO

--INSERT
CREATE PROCEDURE [dbo].[spInsertUsuario](
    @nome VARCHAR (50),
    @login VARCHAR(50),
	@senha VARCHAR(50),
    @email VARCHAR(100),
    @telefone INT,
    @sexo CHAR(1),
	@cpf VARCHAR(50),
	@endereco VARCHAR(100)
)AS
BEGIN
    INSERT INTO tb_usuario(login, senha, nome, email, sexo, telefone, cpf, dataCadastro, endereco)
    OUTPUT inserted.codigo
    VALUES (@login, @senha, @nome, @email, @sexo, @telefone, @cpf, GETDATE(), @endereco)
END

GO

--LISTA
CREATE PROCEDURE [dbo].[spListaFeedback]
AS
BEGIN
    SELECT codigo, usuario, comentario, data
    FROM tb_feedback
END

GO
 
--LISTA
CREATE PROCEDURE [dbo].[spListaServico]
AS
BEGIN
    SELECT s.codigo, s.tipoServico, s.anuncio, s.codigoUsuario, s.preco, s.descricao, s.dataCriacao, s.dataAlteracao, t.nomeServico 
	FROM tb_servico as s
	INNER JOIN tb_tipo_servico as t
	ON s.tipoServico = t.codigo
END

GO

--======================TIPO-SERVIÇO

CREATE PROCEDURE [dbo].[spListaTipoServico]
AS
BEGIN
	SELECT Codigo, nomeServico FROM tb_tipo_servico
END

--======================AGENDA CRIAR TRIGGER E PROCEDURES
GO

INSERT INTO tb_usuario (login, senha, nome, email, sexo, telefone, cpf, endereco) VALUES ('administrador', 'administrador123', 'Administrador', 'administrador@gmail.com', 'O', '43243243', '71555631533', 'Nada')
GO
INSERT INTO tb_usuario (login, senha, nome, email, sexo, telefone, cpf, endereco) VALUES ('Ana', '123', 'Ana Nicole', 'ana@gmail.com', 'F', '43243243', '71555631533', 'Nada')
GO
INSERT INTO tb_usuario (login, senha, nome, email, sexo, telefone, cpf, endereco) VALUES ('Carolina', '123456', 'Carolina', 'carol@gmail.com', 'F', '43243243', '71555631533', 'Nada')

GO

--LISTA
CREATE PROCEDURE [dbo].[spListaUsuario]
AS
BEGIN
    SELECT codigo, login, senha, nome, email, sexo, telefone, endereco, dataCadastro
	FROM tb_usuario
END

GO

CREATE PROCEDURE [dbo].[spLoginUsuario](
    @login VARCHAR(50),
    @senha VARCHAR(20)
)AS
BEGIN
    SELECT TOP 1 codigo, login, senha , nome, dataCadastro
    FROM tb_usuario
    WHERE Login = @login AND senha = @senha
END
GO

--PESQUISA
CREATE PROCEDURE [dbo].[spPesquisaServico](
    @pesquisa VARCHAR(50) = ''
)AS
 
BEGIN
	SELECT s.codigo, s.tipoServico, s.anuncio, s.codigoUsuario, s.preco, s.descricao, s.dataCriacao, s.dataAlteracao, t.nomeServico 
	FROM tb_servico as s
	INNER JOIN tb_tipo_servico as t
	ON s.tipoServico = t.codigo
    WHERE (anuncio LIKE '%' + @pesquisa + '%'
    OR descricao LIKE '%' + @pesquisa + '%')
END

GO

--UPDATE
CREATE PROCEDURE [dbo].[spUpdateFeedback](
	@codigo INT,
    @comentario VARCHAR(500)
)AS
BEGIN
    UPDATE tb_feedback
    SET comentario = @comentario, 
        data = GETDATE()
    WHERE codigo = @codigo
     
END

GO

--UPDATE
CREATE PROCEDURE [dbo].[spUpdateServico](
	@codigo INT,
    @tipoServico INT,
    @anuncio VARCHAR(50),
    @codigoUsuario INT,
    @preco DECIMAL(18,0),
    @descricao VARCHAR(500)
)AS
BEGIN
    UPDATE tb_servico
    SET tipoServico = @tipoServico , 
        anuncio = @anuncio, 
		codigoUsuario = @codigoUsuario,
        preco = @preco, 
		descricao = @descricao,
        dataAlteracao = GETDATE()
    WHERE codigo = @codigo
     
END

GO

--UPDATE
CREATE PROCEDURE [dbo].[spUpdateUsuario](
    @codigo INT,
    @nome VARCHAR (50),
    @login VARCHAR(50),
	@senha VARCHAR(50),
    @email VARCHAR(100),
    @telefone INT,
    @sexo CHAR(1),
	@cpf VARCHAR(50),
	@endereco VARCHAR(100)
)AS
BEGIN
    UPDATE tb_usuario 
    SET login = @login,
		senha = @senha,
        nome = @nome, 
		email = @email,
        telefone = @telefone, 
		sexo = @sexo,
		CPF = @CPF, 
		endereco = @endereco,
        dataAlteracao = GETDATE()
    WHERE codigo = @codigo
END
GO

CREATE TRIGGER trgAgendaUsuario
ON tb_servico
FOR INSERT
AS
BEGIN
DECLARE
@codigoUsuario INT,
@anuncio VARCHAR(50),
@preco DECIMAL (18, 0)
SELECT @codigoUsuario = codigoUsuario, @anuncio= anuncio, @preco = preco FROM INSERTED
INSERT INTO tb_agenda_servico (anuncio, codigoUsuario, preco, data) VALUES (@anuncio, @codigoUsuario, @preco, GETDATE())
END
GO
INSERT INTO tb_servico (codigoUsuario, anuncio, tipoServico, preco, descricao) VALUES (1, 'Frete', 7, 100, 'ACEITAMOS CARTÕES! TEMOS MONTADOR!
Faço Fretes e Mudanças. Venha fazer seu frete conosco. Atendemos todos os bairros. Trabalho profissional e sem dor de cabeça. Motorista Pontual e experiente. Temos Ajudante e montador opcional. Temos mantas e cobertores para proteger seus móveis e eletrodomésticos. Preços populares. Ligue e peça seu orçamento sem compromisso. TEL: XXXX-XXXXX')
GO
INSERT INTO tb_servico (codigoUsuario, anuncio, tipoServico, preco, descricao) VALUES (1, 'Racks de madeira', 4, 70, 'Móvel em ótimos e ótimo preço. Excelente para quem deseja um móvel muito funcional e de qualidade com preço de banana.
Medindo 1,50 X 1,10 X 0,50.')
GO
INSERT INTO tb_servico (codigoUsuario, anuncio, tipoServico, preco, descricao) VALUES (1, 'Kit Monterrey', 4, 70, 'Temos móveis para toda casa. 
Direto da fábrica. 
WhatsApp XXX-XXXX')
GO

CREATE PROCEDURE [dbo].[spBuscaAgenda](
@codigo INT
)AS
BEGIN
    SELECT codigo, anuncio, codigoUsuario, preco, data
	FROM tb_agenda_servico 
	WHERE codigoUsuario = @codigo
END
GO

SELECT * FROM tb_agenda_servico

SELECT * FROM tb_usuario

SELECT * FROM tb_servico

GO

create PROCEDURE [dbo].spBuscaFeedback(
@codigo int

)AS
BEGIN
    SELECT codigo, comentario, usuario, data
	FROM tb_feedback 
	WHERE codigo = @codigo
END
GO
