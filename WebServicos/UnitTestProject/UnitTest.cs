﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using webservicos;

namespace UnitTestProject
{
    [TestClass]
    public class UnitTest
    {
        [TestMethod]
        public void CPFsem11digitos()
        {
            webservicos.Models.Usuario cpfTeste = new webservicos.Models.Usuario();

            cpfTeste.CPF = "09334";

            Assert.AreEqual(false, cpfTeste.IsCpf());
  
        }

        [TestMethod]
        public void CPFinvalido()
        {
            webservicos.Models.Usuario cpfTeste = new webservicos.Models.Usuario();

            cpfTeste.CPF = "09334134564";

            Assert.AreEqual(false, cpfTeste.IsCpf());
        }

        [TestMethod]
        public void CPFvalidoSemPontuacao()
        {
            webservicos.Models.Usuario cpfTeste = new webservicos.Models.Usuario();

            cpfTeste.CPF = "24322623794";

            Assert.AreEqual(true, cpfTeste.IsCpf());
        }

        [TestMethod]
        public void CPFvalidoComPontuacao()
        {
            webservicos.Models.Usuario cpfTeste = new webservicos.Models.Usuario();

            cpfTeste.CPF = Convert.ToString("155.763.197-29");

            Assert.AreEqual(true, cpfTeste.IsCpf());
        }
    }
}
