﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using webservicos.Models;

namespace webservicos.Controllers
{
    public class FeedbackController : Controller
    {
        //
        // GET: /Feedback/
       
        public ActionResult Index(string pesquisa)
        {
            try
            {
                var lst = new FeedbackDAO().Listar();
                return View(lst);
            }
            catch (Exception ex)
            {
                TempData["ErrorMsg"] = String.Format("Falha ao buscar feedbacks. {0}", ex.Message);
                return View(new List<Feedback>());
            }
        }
        [Authorize]
        public ActionResult Cadastro()
        {
            return View();
        }
        [Authorize(Roles =  "Administrador")]
        public ActionResult Editar(int? id)
        {
            if (id == null || id == 0)
                return View();

            try
            {
                var obj = new FeedbackDAO().Buscar((int)id);
                return View(obj);
            }
            catch (KeyNotFoundException ex)
            {
                TempData["ErrorMsg"] = ex.Message;
                return View();
            }
            catch (Exception ex)
            {
                TempData["ErrorMsg"] = String.Format("Não foi possivel buscar o feedback. {0}", ex.Message);
                return View();
            }
        }
        [Authorize]
        public ActionResult Salvar(Feedback obj)
        {
            if (!ModelState.IsValid)
            {
                return View("Cadastro", obj);
            }

            try
            {
                var usuario = (Usuario)Session["usuario"];

                new FeedbackDAO().Salvar(obj, usuario);

                TempData["SucessMsg"] = "Feedback salvo com sucesso!";
            }
            catch (Exception ex)
            {
                TempData["ErrorMsg"] = String.Format("Falha ao salvar o feedback. {0}", ex.Message);
            }
            //return View("Editar", obj); 
            return RedirectToAction("Index");
        }
        [Authorize(Roles = "Administrador")]
        public ActionResult Excluir(int id)
        {
            try
            {
                var usuario = (Usuario)Session["usuario"];

                new FeedbackDAO().Delete(new Feedback { Codigo = id }, usuario);
                TempData["SucessMsg"] = "Feedback excluído com sucesso.";
            }
            catch (Exception ex)
            {
                TempData["ErrorMsg"] = String.Format("Falha ao excluir feedback. {0}", ex.Message);
            }

            return RedirectToAction("Index");
        }
        public ActionResult Detalhes(int id)
        {
            try
            {
                var obj = new FeedbackDAO().Buscar(id);
                return View(obj);
            }
            catch (KeyNotFoundException ex)
            {
                TempData["ErrorMsg"] = ex.Message;
                return View();
            }
            catch (Exception ex)
            {
                TempData["ErrorMsg"] = String.Format("Não foi possivel buscar o Feedback. {0}", ex.Message);
                return View();
            }
        }
    }
}
