﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using webservicos.Models;

namespace webservicos.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(Usuario obj)
        {

            try
            {
                var login = new UsuarioDAO().Autenticar(obj);

                if (!login)
                    throw new Exception("Cadastro e/ou senha inválidos.");
             
                FormsAuthentication.SetAuthCookie(obj.Login, false);

                Response.Cookies.Add(new HttpCookie("idUsuario", obj.Codigo.ToString()));

                Session["usuario"] = obj;
                TempData["SucessMsg"] = "Logado com sucesso!";
            return View("Index", obj);
            }

            catch (Exception ex)
            {
                TempData["ErrorMsg"] = string.Format("Falha ao fazer login. {0}", ex.Message);
                return View("Login", obj);
            }

        }

        public ActionResult Logout()
        {
            //Sai do usuário e da sessão
            FormsAuthentication.SignOut();
            Session.Abandon();
            return View("Login");
        }
    }
}
