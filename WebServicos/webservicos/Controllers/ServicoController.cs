﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using webservicos.Models;

namespace webservicos.Controllers
{
    public class ServicoController : Controller
    {
        // GET: Servico
        public ActionResult Index(string pesquisa)
        {
            try
            {
                var lst = new ServicoDAO().Listar();
                return View(lst);
            }
            catch (Exception ex)
            {
                TempData["ErrorMsg"] = String.Format("Falha ao buscar serviços. {0}", ex.Message);
                return View(new List<Servico>());
            }
        }

        [Authorize]
        public ActionResult Cadastro()
        {
            var lista = new TipoServicoDAO().Listar();
            ViewBag.TipoServico = lista;

            return View();
        }

        [Authorize]
        public ActionResult Editar(int? id)
        {
            var lista = new TipoServicoDAO().Listar();
            ViewBag.TipoServico = lista;

            if (id == null || id == 0)
                return View();

            try
            {
                var obj = new ServicoDAO().Buscar((int)id);
                return View(obj);
            }
            catch (KeyNotFoundException ex)
            {
                TempData["ErrorMsg"] = ex.Message;
                return View();
            }
            catch (Exception ex)
            {
                TempData["ErrorMsg"] = String.Format("Não foi possivel buscar o serviço. {0}", ex.Message);
                return View();
            }
        }

        [Authorize]
        public ActionResult Salvar(Servico obj)
        {
            var lista = new TipoServicoDAO().Listar();
            ViewBag.TipoServico = lista;

            if (!ModelState.IsValid)
            {
                return View("Cadastro", obj);
            }
            try
            {
                var usuario = (Usuario)Session["usuario"];
                new ServicoDAO().Salvar(obj, usuario);
                TempData["SucessMsg"] = "Servico salvo com sucesso!";
            }
            catch (Exception ex)
            {
                TempData["ErrorMsg"] = String.Format("Falha ao salvar o serviço. {0}", ex.Message);
            }
            //return View("Editar", obj); 
            return RedirectToAction("Index");
        }

        [Authorize]
        public ActionResult Excluir(int id)
        {
            var lista = new TipoServicoDAO().Listar();
            ViewBag.TipoServico = lista;
            try
            {
                var usuario = (Usuario)Session["usuario"];
                new ServicoDAO().Delete(new Servico { Codigo = id }, usuario);
                TempData["SucessMsg"] = "Serviço excluído com sucesso.";
            }
            catch (Exception ex)
            {
                TempData["ErrorMsg"] = String.Format("Falha ao excluir serviço. {0}", ex.Message);
            }

            return RedirectToAction("Index");
        }
        public ActionResult Detalhes(int id)
        {
            try
            {
                var obj = new ServicoDAO().Buscar(id);
                return View(obj);
            }
            catch (KeyNotFoundException ex)
            {
                TempData["ErrorMsg"] = ex.Message;
                return View();
            }
            catch (Exception ex)
            {
                TempData["ErrorMsg"] = String.Format("Não foi possivel buscar o servico. {0}", ex.Message);
                return View();
            }
        }

        public ActionResult Pesquisar(string pesquisa)
        {
            try
            {
                var lst = new ServicoDAO().Pesquisar(pesquisa);
                return View(lst);
            }
            catch (Exception ex)
            {
                TempData["ErrorMsg"] = String.Format("Falha ao buscar serviços. {0}", ex.Message);
                return View(new List<Servico>());
            }
        }
    }
}