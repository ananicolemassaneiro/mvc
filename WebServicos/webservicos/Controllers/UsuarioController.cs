﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using webservicos.Models;

namespace webservicos.Controllers
{
    public class UsuarioController : Controller
    {
        // GET: Usuario
        public ActionResult Index(int? codigo)
        {
            try
            {        
                var lst = new UsuarioDAO().Listar();
                return View(lst);
            }
            catch (Exception ex)
            {
                TempData["ErrorMsg"] = String.Format("Falha ao buscar usuários. {0}", ex.Message);
                return View(new List<Usuario>());

            }
        }

        public ActionResult Cadastro()
        {
            return View();
        }
        [Authorize]
        public ActionResult Editar(int? id)
        {
            if (id == null || id == 0)
                return View();

            try
            {
                var usuario = (Usuario)Session["Usuario"];
                var obj = new UsuarioDAO().Buscar((int)id);
                return View(obj);
            }
            catch (KeyNotFoundException ex)
            {
                TempData["ErrorMsg"] = ex.Message;
                return View();
            }
            catch (Exception ex)
            {
                TempData["ErrorMsg"] = String.Format("Não foi possivel buscar o usuário. {0}", ex.Message);
                return View();
            }
        }
        [Authorize]
        public ActionResult Salvar(Usuario obj)
        {
            if (!obj.IsCpf())
            {
                ModelState.AddModelError("CPF", "CPF inválido.");
            }
            if (!ModelState.IsValid)
            {
                return View("Cadastro", obj);
            }
            try
            {
                var usuario = (Usuario)Session["usuario"];
                new UsuarioDAO().Salvar(obj, usuario);
                TempData["SucessMsg"] = "Usuário salvo com sucesso!";
            }
            catch (Exception ex)
            {
                TempData["ErrorMsg"] = String.Format("Falha ao salvar o usuário. {0}", ex.Message);
            }
            return RedirectToAction("Editar", new { @id = obj.Codigo });
        }
        [Authorize]
        public ActionResult Excluir(int id)
        {
            try
            {
                var usuario = (Usuario)Session["usuario"];
                new UsuarioDAO().Delete(new Usuario { Codigo = id }, usuario);
                TempData["SucessMsg"] = "Usuário excluído com sucesso.";
            }
            catch (Exception ex)
            {
                TempData["ErrorMsg"] = String.Format("Falha ao excluir usuário. {0}", ex.Message);
            }

            return RedirectToAction("Index");
        }
        [Authorize]
        public ActionResult Perfil(int? codigo)
        {
           
            var usuario = (Usuario)Session["usuario"];
            codigo = usuario.Codigo;

            try
            {
                
                var obj = new UsuarioDAO().Buscar((int)codigo);
                return View(obj);
            }
            catch (KeyNotFoundException ex)
            {
                TempData["ErrorMsg"] = ex.Message;
                return View();
            }
            catch (Exception ex)
            {
                TempData["ErrorMsg"] = String.Format("Não foi possivel buscar o usuário. {0}", ex.Message);
                return View();
            }
        }

        public ActionResult Agenda(int? codigo)
        {
            var usuario = (Usuario)Session["usuario"];
            codigo = usuario.Codigo;

            try
            {
                IEnumerable<Agenda> lst = new AgendaDAO().Buscar((int)codigo);
                return View(lst);
            }
            catch (Exception ex)
            {
                TempData["ErrorMsg"] = String.Format("Falha ao buscar serviços. {0}", ex.Message);
                return View(new List<Agenda>());

            }
        }
    }
}
