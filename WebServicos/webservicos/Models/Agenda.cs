﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace webservicos.Models
{
    public class Agenda
    {
        [Key]
        public int Codigo { get; set; }

        [Display(Name = "Código do Usuário")]
        public int CodigoUsuario { get; set; }

        [Required]
        [Display(Name = "Anúncio")]
        public string Anuncio { get; set; }

        [Required]
        [Display(Name = "Preço")]
        public decimal Preco { get; set; }

        public DateTime Data { get; set; }
    }
}