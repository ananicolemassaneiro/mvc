﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace webservicos.Models
{
    public class AgendaDAO
    {
        public List<Agenda> Buscar(int codigo)
        {
            var conex = new ConectionFactory();
            string sp = "spBuscaAgenda";

            var parametros = new List<SqlParameter>();
            parametros.Add(new SqlParameter("@codigo", codigo));

            var dt = conex.ExecutaSpDataTable(sp, parametros);

            var agendas = new List<Agenda>();

            for (var i = 0; i < dt.Rows.Count; i++)
            {
                var agenda = new Agenda();

                if (dt.Rows.Count < 1)
                    throw new Exception("Serviço não encontrado.");

                agenda.Codigo = Convert.ToInt32(dt.Rows[i]["codigo"]);
                agenda.Anuncio = dt.Rows[i]["anuncio"].ToString();
                agenda.CodigoUsuario = Convert.ToInt32(dt.Rows[i]["codigoUsuario"]);
                agenda.Preco = Convert.ToDecimal(dt.Rows[i]["preco"]);
                agenda.Data = Convert.ToDateTime(dt.Rows[i]["data"]);

                agendas.Add(agenda);
            }
            return agendas;
        }
    }
}