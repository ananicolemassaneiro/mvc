﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace webservicos.Models
{
    public class Feedback
    {
        [Key]
        public int Codigo { get; set; }

        [Required]
        [Display(Name = "Nome")]
        public string Autor { get; set; }

        [Required]
        public string Comentario { get; set; }

        public DateTime Data { get; set; }

    }
}