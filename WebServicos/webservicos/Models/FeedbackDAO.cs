﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace webservicos.Models
{
    public class FeedbackDAO : IDAO<Feedback>
    {
        public Feedback Buscar(int codigo)
        {
            var conex = new ConectionFactory();
            string sp = "spBuscaFeedback";

            var parametros = new List<SqlParameter>();
            parametros.Add(new SqlParameter("@codigo", codigo));

            var dt = conex.ExecutaSpDataTable(sp, parametros);

            if (dt.Rows.Count < 1)
                throw new Exception("Serviço não encontrado.");

            var feed = new Feedback();

            feed.Codigo = Convert.ToInt32(dt.Rows[0]["codigo"]);
            feed.Autor = dt.Rows[0]["usuario"].ToString();
            feed.Comentario = dt.Rows[0]["comentario"].ToString();
           
            return feed;
        }

        public void Delete(Feedback obj, Usuario usuario)
        {
            var conex = new ConectionFactory();
            string sp = "spDeleteFeedback";

            var parametros = new List<SqlParameter>();
            parametros.Add(new SqlParameter("@codigo", obj.Codigo));

            conex.ExecutaNonQuerySP(sp, parametros);
        }

        public void Insert(Feedback obj, Usuario usuario)
        {
            var conex = new ConectionFactory();
            string sp = "spInsertFeedback";

            var parametros = new List<SqlParameter>();
            parametros.Add(new SqlParameter("@comentario", obj.Comentario));
            parametros.Add(new SqlParameter("@usuario", obj.Autor));

            conex.ExecutaNonQuerySP(sp, parametros);

        }

        public List<Feedback> Listar()
        {
            var conex = new ConectionFactory();
            string sp = "spListaFeedback";

            var dt = conex.ExecutaSpDataTable(sp);
            var feedbacks = new List<Feedback>();

            for (var i = 0; i < dt.Rows.Count; i++)
            {
                var feedback = new Feedback();

                if (dt.Rows.Count < 1)
                    throw new Exception("Feedback não encontrado.");
                feedback.Codigo = int.Parse(dt.Rows[i]["codigo"].ToString());
                feedback.Comentario = dt.Rows[i]["comentario"].ToString();
                feedback.Autor = dt.Rows[i]["usuario"].ToString();
                feedback.Data = Convert.ToDateTime(dt.Rows[i]["data"]);

                feedbacks.Add(feedback);
            }
            return feedbacks;
        }

        public List<Feedback> Pesquisar(string pesquisa)
        {
            throw new NotImplementedException();
        }

        public void Salvar(Feedback obj, Usuario usuario)
        {
            if (obj.Codigo == 0)
                Insert(obj, usuario);
            else
                Update(obj, usuario);
        }

        public void Update(Feedback obj, Usuario usuario)
        {
            var conex = new ConectionFactory();
            string sp = "spUpdateFeedback";

            var parametros = new List<SqlParameter>();
            parametros.Add(new SqlParameter("@codigo", obj.Codigo));
            parametros.Add(new SqlParameter("@comentario", obj.Comentario));

            conex.ExecutaNonQuerySP(sp, parametros);
        }
    }
}