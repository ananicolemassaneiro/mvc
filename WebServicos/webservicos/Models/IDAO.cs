﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webservicos.Models
{
    public interface IDAO<T>
    {
        void Salvar(T obj, Usuario usuario);
        void Insert(T obj, Usuario usuario);
        void Delete(T obj, Usuario usuario);
        void Update(T obj, Usuario usuario);
        T Buscar(int codigo);
        List<T> Listar();
        List<T> Pesquisar(string pesquisa);
    }
}