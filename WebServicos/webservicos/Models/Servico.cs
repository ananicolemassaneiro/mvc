﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace webservicos.Models
{
    public class Servico
    {

        public int Codigo { get; set; }

        [Display(Name = "Tipo de Serviço")]
        public TipoServico TipoServico { get; set; }

        [Required]
        [Display(Name = "Anúncio")]
        public string Anuncio { get; set; }

        [Display(Name = "Usuário")]
        public string CodigoUsuario { get; set; }

        [Required]
        [Display(Name = "Preço")]
        public decimal Preco { get; set; }

        [Required]
        [Display(Name = "Descrição")]
        public string Descricao { get; set; }

        [Display(Name = "Data de Criação")]
        public DateTime DataCriacao { get; set; }

        public DateTime DataAlteracao { get; set; }
    }

    
}