﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data;

namespace webservicos.Models
{
    public class ServicoDAO : IDAO<Servico>
    {
        public Servico Buscar(int codigo)
        {
            var conex = new ConectionFactory();
            string sp = "spBuscaServico";

            var parametros = new List<SqlParameter>();
            parametros.Add(new SqlParameter("@codigo", codigo));

            var dt = conex.ExecutaSpDataTable(sp, parametros);

            if (dt.Rows.Count < 1)
                throw new Exception("Serviço não encontrado.");

            var servico = new Servico();
            var tipoServico = new TipoServico();

            servico.Codigo = Convert.ToInt32(dt.Rows[0]["codigo"]);
            tipoServico.Codigo = Convert.ToInt32(dt.Rows[0]["tipoServico"]);
            //servico.TipoServico = Convert.ToInt32(dt.Rows[0]["tipoServico"]);
            servico.Anuncio = dt.Rows[0]["anuncio"].ToString();
            servico.Descricao = dt.Rows[0]["descricao"].ToString();
            servico.CodigoUsuario = dt.Rows[0]["codigoUsuario"].ToString();
            servico.Preco = Convert.ToInt32(dt.Rows[0]["preco"]);


            return servico;
        }

        public void Delete(Servico obj, Usuario usuario)
        {
            var conex = new ConectionFactory();
            string sp = "spDeleteServico";

            var parametros = new List<SqlParameter>();
            parametros.Add(new SqlParameter("@codigo", obj.Codigo));
            //parametros.Add(new SqlParameter("@codigoUsuario", usuario.Codigo));

            conex.ExecutaNonQuerySP(sp, parametros);
        }

        public void Insert(Servico obj, Usuario usuario)
        {
            var conex = new ConectionFactory();
            string sp = "spInsertServico";

            var parametros = new List<SqlParameter>();
            parametros.Add(new SqlParameter("@tipoServico", obj.TipoServico.Codigo));
            parametros.Add(new SqlParameter("@anuncio", obj.Anuncio));
            parametros.Add(new SqlParameter("@codigoUsuario", usuario.Codigo));
            parametros.Add(new SqlParameter("@preco", obj.Preco));
            parametros.Add(new SqlParameter("@descricao", obj.Descricao));

            conex.ExecutaScalarSP(sp, parametros);
        }

        public List<Servico> Listar()
        {
            var conex = new ConectionFactory();
            string sp = "spListaServico";

            var dt = conex.ExecutaSpDataTable(sp);
            var servicos = new List<Servico>();

            for (var i = 0; i < dt.Rows.Count; i++)
            {
                var servico = new Servico();
                var tipoServico = new TipoServico();

                if (dt.Rows.Count < 1)
                    throw new Exception("Serviço não encontrado.");

                servico.Codigo = Convert.ToInt32(dt.Rows[i]["codigo"]);
                tipoServico.Codigo = Convert.ToInt32(dt.Rows[i]["tipoServico"].ToString());
                tipoServico.NomeServico = dt.Rows[i]["nomeServico"].ToString();
                servico.TipoServico = tipoServico;
                servico.Anuncio = dt.Rows[i]["anuncio"].ToString();
                servico.CodigoUsuario = dt.Rows[i]["codigoUsuario"].ToString();
                servico.Preco = Convert.ToDecimal(dt.Rows[i]["preco"]);
                servico.Descricao = dt.Rows[i]["descricao"].ToString();
                servico.DataCriacao = Convert.ToDateTime(dt.Rows[i]["dataCriacao"]);
                servico.DataAlteracao = Convert.ToDateTime(dt.Rows[i]["dataAlteracao"]);

                servicos.Add(servico);
            }
            return servicos;
        }

        public List<Servico> Pesquisar(string pesquisa)
        {
            var conex = new ConectionFactory();
            string sp = "spPesquisaServico";

            var parametros = new List<SqlParameter>();
            parametros.Add(new SqlParameter("@pesquisa", pesquisa));

            var dt = conex.ExecutaSpDataTable(sp, parametros);
            var servicos = new List<Servico>();

            for (var i = 0; i < dt.Rows.Count; i++)
            {
                var servico = new Servico();
                var tipoServico = new TipoServico();

                if (dt.Rows.Count < 1)
                    throw new Exception("Serviço não encontrado.");

                servico.Codigo = Convert.ToInt32(dt.Rows[i]["codigo"]);
                tipoServico.Codigo = Convert.ToInt32(dt.Rows[i]["tipoServico"].ToString());
                tipoServico.NomeServico = dt.Rows[i]["nomeServico"].ToString();
                servico.TipoServico = tipoServico;
                servico.Anuncio = dt.Rows[i]["anuncio"].ToString();
                servico.CodigoUsuario = dt.Rows[i]["codigoUsuario"].ToString();
                servico.Preco = Convert.ToDecimal(dt.Rows[i]["preco"]);
                servico.Descricao = dt.Rows[i]["descricao"].ToString();
                servico.DataCriacao = Convert.ToDateTime(dt.Rows[i]["dataCriacao"]);
                servico.DataAlteracao = Convert.ToDateTime(dt.Rows[i]["dataAlteracao"]);

                servicos.Add(servico);
            }
            return servicos;
        }

        public void Salvar(Servico obj, Usuario usuario)
        {
            if (obj.Codigo == 0)
                Insert(obj, usuario);
            else
                Update(obj, usuario);
        }

        public void Update(Servico obj, Usuario usuario)
        {
            var conex = new ConectionFactory();
            string sp = "spUpdateServico";

            var parametros = new List<SqlParameter>();
            parametros.Add(new SqlParameter("@codigo", obj.Codigo));
            //parametros.Add(new SqlParameter("@Codigo", tipoServico.Codigo));
            //parametros.Add(new SqlParameter("@nomeServico", tipoServico.NomeServico));
            parametros.Add(new SqlParameter("@tipoServico", obj.TipoServico.Codigo));
            parametros.Add(new SqlParameter("@anuncio", obj.Anuncio));
            parametros.Add(new SqlParameter("@codigoUsuario", usuario.Codigo));
            parametros.Add(new SqlParameter("@preco", obj.Preco));
            parametros.Add(new SqlParameter("@descricao", obj.Descricao));
          
            
            conex.ExecutaNonQuerySP(sp, parametros);
        }

        Servico IDAO<Servico>.Buscar(int codigo)
        {
            throw new NotImplementedException();
        }

        void IDAO<Servico>.Delete(Servico obj, Usuario usuario)
        {
            throw new NotImplementedException();
        }

        void IDAO<Servico>.Insert(Servico obj, Usuario usuario)
        {
            throw new NotImplementedException();
        }

        List<Servico> IDAO<Servico>.Listar()
        {
            throw new NotImplementedException();
        }

        List<Servico> IDAO<Servico>.Pesquisar(string pesquisa)
        {
            throw new NotImplementedException();
        }

        void IDAO<Servico>.Salvar(Servico obj, Usuario usuario)
        {
            throw new NotImplementedException();
        }

        void IDAO<Servico>.Update(Servico obj, Usuario usuario)
        {
            throw new NotImplementedException();
        }
    }
}