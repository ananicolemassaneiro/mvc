﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webservicos.Models
{
    public class TipoServicoDAO : IDAO<TipoServico>
    {
        public TipoServico Buscar(int codigo)
        {
            throw new NotImplementedException();
        }

        public void Delete(TipoServico obj, Usuario usuario)
        {
            throw new NotImplementedException();
        }

        public void Insert(TipoServico obj, Usuario usuario)
        {
            throw new NotImplementedException();
        }

        public List<TipoServico> Listar()
        {
            var conex = new ConectionFactory();
            string sp = "spListaTipoServico";

            var dt = conex.ExecutaSpDataTable(sp);
            var tipoServicos = new List<TipoServico>();

            for (var i = 0; i < dt.Rows.Count; i++)
            {
                var tipoServico = new TipoServico();

                if (dt.Rows.Count < 1)
                    throw new Exception("Tipo de serviço não encontrado.");
                tipoServico.Codigo = Convert.ToInt32(dt.Rows[i]["Codigo"]);
                tipoServico.NomeServico = dt.Rows[i]["nomeServico"].ToString();

                tipoServicos.Add(tipoServico);
            }
            return tipoServicos;
        }

        public List<TipoServico> Pesquisar(string pesquisa)
        {
            throw new NotImplementedException();
        }

        public void Salvar(TipoServico obj, Usuario usuario)
        {
            throw new NotImplementedException();
        }

        public void Update(TipoServico obj, Usuario usuario)
        {
            throw new NotImplementedException();
        }
    }
}