﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace webservicos.Models
{
    public class UsuarioDAO : IDAO<Usuario>
    {
        public Usuario Buscar(int codigo)
        {
            var conex = new ConectionFactory();
            string sp = "spBuscaUsuario";

            var parametros = new List<SqlParameter>();
            parametros.Add(new SqlParameter("@codigo", codigo));

            var dt = conex.ExecutaSpDataTable(sp, parametros);

            if (dt.Rows.Count < 1)
                throw new Exception("Usuário não encontrado.");

            var usuario = new Usuario
            (
                dt.Rows[0]["Nome"].ToString(),
                dt.Rows[0]["Login"].ToString()
            );
            usuario.Login = dt.Rows[0]["login"].ToString();
            usuario.Senha = dt.Rows[0]["senha"].ToString();
            usuario.Codigo = Convert.ToInt32(dt.Rows[0]["codigo"]);
            usuario.Telefone = Convert.ToInt32(dt.Rows[0]["telefone"]);
            usuario.Email = dt.Rows[0]["email"].ToString();
            usuario.Sexo = dt.Rows[0]["sexo"].ToString();
            usuario.CPF = dt.Rows[0]["CPF"].ToString();
            usuario.Endereco = dt.Rows[0]["endereco"].ToString();
            usuario.DataCadastro = Convert.ToDateTime(dt.Rows[0]["dataCadastro"]);

            return usuario;
        }

        public void Delete(Usuario obj, Usuario usuario)
        {
            var conex = new ConectionFactory();
            string sp = "spDeleteUsuario";

            var parametros = new List<SqlParameter>();
            parametros.Add(new SqlParameter("@codigo", obj.Codigo));

            conex.ExecutaNonQuerySP(sp, parametros);
        }

        public void Insert(Usuario obj, Usuario usuario)
        {
            var conex = new ConectionFactory();
            string sp = "spInsertUsuario";

            var parametros = new List<SqlParameter>();
            parametros.Add(new SqlParameter("@login", obj.Login));
            parametros.Add(new SqlParameter("@senha", obj.Senha));
            parametros.Add(new SqlParameter("@nome", obj.Nome));
            parametros.Add(new SqlParameter("@telefone", obj.Telefone));
            parametros.Add(new SqlParameter("@email", obj.Email));
            parametros.Add(new SqlParameter("@sexo", obj.Sexo));
            parametros.Add(new SqlParameter("@CPF", obj.CPF));
            parametros.Add(new SqlParameter("@endereco", obj.Endereco));

            obj.Codigo = (int)conex.ExecutaScalarSP(sp, parametros);
        }

        public List<Usuario> Listar()
        {
            var conex = new ConectionFactory();
            string sp = "spListaUsuario";

            var dt = conex.ExecutaSpDataTable(sp);
            var usuarios = new List<Usuario>();

            for (var i = 0; i < dt.Rows.Count; i++)
            {
                var usuario = new Usuario();

                if (dt.Rows.Count < 1)
                    throw new Exception("Usuários não encontrados.");

                usuario.Login = dt.Rows[i]["login"].ToString();
                usuario.Senha = dt.Rows[i]["senha"].ToString();
                usuario.Codigo = Convert.ToInt32(dt.Rows[i]["codigo"].ToString());
                usuario.Nome = dt.Rows[i]["nome"].ToString();
                usuario.Email = dt.Rows[i]["email"].ToString();
                usuario.Sexo = dt.Rows[i]["sexo"].ToString();
                usuario.Telefone = Convert.ToInt32(dt.Rows[i]["telefone"]);
                usuario.Endereco = dt.Rows[i]["endereco"].ToString();
                usuario.DataCadastro = Convert.ToDateTime(dt.Rows[i]["dataCadastro"]);

                usuarios.Add(usuario);
            }
            return usuarios;
        }

        public List<Usuario> Pesquisar(string pesquisa)
        {
            throw new NotImplementedException();
        }

        public void Salvar(Usuario obj, Usuario usuario)
        {
            if (obj.Codigo == 0)
                Insert(obj, usuario);
            else
                Update(obj, usuario);
        }

        public void Update(Usuario obj, Usuario usuario)
        {
            var conex = new ConectionFactory();
            string sp = "spUpdateUsuario";

            var parametros = new List<SqlParameter>();
            parametros.Add(new SqlParameter("@codigo", obj.Codigo));
            parametros.Add(new SqlParameter("@login", obj.Login));
            parametros.Add(new SqlParameter("@senha", obj.Senha));
            parametros.Add(new SqlParameter("@nome", obj.Nome));
            parametros.Add(new SqlParameter("@telefone", obj.Telefone));
            parametros.Add(new SqlParameter("@email", obj.Email));
            parametros.Add(new SqlParameter("@sexo", obj.Sexo));
            parametros.Add(new SqlParameter("@CPF", obj.CPF));
            parametros.Add(new SqlParameter("@endereco", obj.Endereco));

            conex.ExecutaNonQuerySP(sp, parametros);
        }

        public bool Autenticar(Usuario obj)
        {
            var conex = new ConectionFactory();
            string sp = "spLoginUsuario";

            var parametros = new List<SqlParameter>
            {
                new SqlParameter ("@login", obj.Login),
                new SqlParameter("@senha", obj.Senha)
            };

            var dt = conex.ExecutaSpDataTable(sp, parametros);

            if (dt.Rows.Count == 0)
                return false;

            obj.Codigo = Convert.ToInt32(dt.Rows[0]["codigo"]);
            obj.Nome = dt.Rows[0]["nome"].ToString();

            return (dt.Rows.Count == 1);
        }
    }
}